<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerHi9ZLKG\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerHi9ZLKG/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerHi9ZLKG.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerHi9ZLKG\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerHi9ZLKG\App_KernelDevDebugContainer([
    'container.build_hash' => 'Hi9ZLKG',
    'container.build_id' => '20cd81a5',
    'container.build_time' => 1585747626,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerHi9ZLKG');
