<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* blog/index.html.twig */
class __TwigTemplate_470f89a69c6c69597884c0a6597c7db87d9cb3c1c39de27c26c82cc52acb6b4c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "blog/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello BlogController!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<section class=\"article\">
    <article>
        <h2>titre de l'article</h2>
        <div class=\"metadata\">ecrit le 10/05/2018 à 19:00 dans la catégorie politique</div>
        <div class=\"content\">
            <img src=\"https://placehold.it/350x150\" alt=\"\">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium officiis, ullam deserunt nesciunt nam praesentium.</p>
            <p>Sequi illum distinctio quae ducimus, inventore minus commodi iste, id numquam sunt deleniti ex quia.</p>
            <a href=\"\" class=\"btn btn-primary\">lire la suite</a>
        </div>
        
    </article>
    <article>
        <h2>titre de l'article</h2>
        <div class=\"metadata\">ecrit le 10/05/2018 à 19:00 dans la catégorie politique</div>
        <div class=\"content\">
            <img src=\"https://placehold.it/350x150\" alt=\"\">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium officiis, ullam deserunt nesciunt nam praesentium.</p>
            <p>Sequi illum distinctio quae ducimus, inventore minus commodi iste, id numquam sunt deleniti ex quia.</p>
            <a href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("blog_show");
        echo "\" class=\"btn btn-primary\">lire la suite</a>
        </div>
        
    </article>
    <article>
        <h2>titre de l'article</h2>
        <div class=\"metadata\">ecrit le 10/05/2018 à 19:00 dans la catégorie politique</div>
        <div class=\"content\">
            <img src=\"https://placehold.it/350x150\" alt=\"\">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium officiis, ullam deserunt nesciunt nam praesentium.</p>
            <p>Sequi illum distinctio quae ducimus, inventore minus commodi iste, id numquam sunt deleniti ex quia.</p>
            <a href=\"";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("blog_show");
        echo "\" class=\"btn btn-primary\">lire la suite</a>
        </div>
        
    </article>
</section>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "blog/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 36,  109 => 25,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello BlogController!{% endblock %}

{% block body %}
<section class=\"article\">
    <article>
        <h2>titre de l'article</h2>
        <div class=\"metadata\">ecrit le 10/05/2018 à 19:00 dans la catégorie politique</div>
        <div class=\"content\">
            <img src=\"https://placehold.it/350x150\" alt=\"\">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium officiis, ullam deserunt nesciunt nam praesentium.</p>
            <p>Sequi illum distinctio quae ducimus, inventore minus commodi iste, id numquam sunt deleniti ex quia.</p>
            <a href=\"\" class=\"btn btn-primary\">lire la suite</a>
        </div>
        
    </article>
    <article>
        <h2>titre de l'article</h2>
        <div class=\"metadata\">ecrit le 10/05/2018 à 19:00 dans la catégorie politique</div>
        <div class=\"content\">
            <img src=\"https://placehold.it/350x150\" alt=\"\">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium officiis, ullam deserunt nesciunt nam praesentium.</p>
            <p>Sequi illum distinctio quae ducimus, inventore minus commodi iste, id numquam sunt deleniti ex quia.</p>
            <a href=\"{{ path('blog_show') }}\" class=\"btn btn-primary\">lire la suite</a>
        </div>
        
    </article>
    <article>
        <h2>titre de l'article</h2>
        <div class=\"metadata\">ecrit le 10/05/2018 à 19:00 dans la catégorie politique</div>
        <div class=\"content\">
            <img src=\"https://placehold.it/350x150\" alt=\"\">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium officiis, ullam deserunt nesciunt nam praesentium.</p>
            <p>Sequi illum distinctio quae ducimus, inventore minus commodi iste, id numquam sunt deleniti ex quia.</p>
            <a href=\"{{ path('blog_show') }}\" class=\"btn btn-primary\">lire la suite</a>
        </div>
        
    </article>
</section>


{% endblock %}
", "blog/index.html.twig", "/home/aida/exercices/test_symfony/test1/templates/blog/index.html.twig");
    }
}
